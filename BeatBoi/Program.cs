﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BeatBoi
{
    class Program
    {
        public static List<string> Sayings = new List<string>()
        {
            "Loading components...",
            "Error retrieving file code: 657",
            "...",
            "Extension failure...rebooting",
            "Tracking Lost!",
            "Warning! It looks like you have assigned a null value to a non-nullable source.",
            "Compiler error, issue on line 153. Unexpected emoji.",
            "User query failed.",
            "Success.",
            "Query to database active.",
            "Incomplete Factor Warning!",
            "Rounding error in component",
            "ERROR! - - RECOVERY FAILED ATTEMPTING SECONDARY BYPASS - - SUCCESS! ^_^",
            "-------------------------------------------------------------------------",
            "//Clear final inputs",
            "//Exchange Byas module for null valued tuple",
            "//perform quicksort",
            "Reorganizing File System...",
            "Vector: (34.44,12.77) - Good Vector",
            "Vector: (357.49,17.54) - Good Vector",
            "Vector: (531.55,122.47) - BAD Vector",
            "Vector: (354.45,172.23) - Good Vector",
            "Vector: (384.44,72.74) - BAD Vector"
        };

        static void Main(string[] args)
        {
            for(int i = 0; i < 1000; i++)
            {
                MasterControlPrinter.TextOuputs.AddRange(Sayings);
            }

            Random R = new Random();
            while (true)
            {
                MasterControlPrinter.Print(Sayings[R.Next(0,Sayings.Count)]);
                //Console.WriteLine("Volume: "+MasterControlPrinter.GetVolume());
            }
        }
    }
}
