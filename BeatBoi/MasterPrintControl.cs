﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using NAudio.CoreAudioApi;

//created by TreeBaron
//this software is in the public domain

namespace BeatBoi
{
    class MasterControlPrinter
    {
        public static MMDeviceEnumerator Enumerator;

        public static MMDevice AudioDevice = null;

        public static float TotalVolume = 1;

        public static int Divisor = 1;

        public static float PreviousValue = 0;

        public static List<string> TextOuputs = new List<string>();

        public static void Print(string a)
        {
            if (TextOuputs.Count < 1000)
            {
                TextOuputs.Add(a);
            }

            //audio stuff
            if (AudioDevice == null)
            {
                Enumerator = new MMDeviceEnumerator();
                AudioDevice = Enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            }

            Divisor++;

            float volume = AudioDevice.AudioMeterInformation.MasterPeakValue;

            TotalVolume += volume;

            float AverageVolume = TotalVolume / Divisor;

            //Todo: Idea color coded messages yellow = warning, red = error, etc...

            if (volume > PreviousValue * 1.75 && TextOuputs.Count >= 1)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                TryPrint(10);
                Console.WriteLine("STACK OVERFLOW!");
            }
            else if (volume > PreviousValue * 1.5 && TextOuputs.Count >= 1)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                TryPrint(7);
            }
            else if (volume > PreviousValue * 1.3 && TextOuputs.Count >= 1)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                TryPrint(3);
            }
            else if (volume > PreviousValue * 1.1 && TextOuputs.Count >= 1)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                TryPrint(2);
            }
            else if (volume > PreviousValue * 1.0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                TryPrint(1);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Black;
                TryPrint(1,false);
            }
            Console.ResetColor();

            Thread.Sleep(31);

            PreviousValue = (volume + AverageVolume)/2;
        }

        private static void TryPrint(int Amount = 1,bool Remove = true)
        {
            for (int i = 0; i < Amount; i++)
            {
                if (TextOuputs.Count >= 1)
                {
                    Console.WriteLine(TextOuputs[TextOuputs.Count - 1]);
                    if (Remove)
                    {
                        TextOuputs.RemoveAt(TextOuputs.Count - 1);
                    }
                }
            }
        }

        public static float GetVolume()
        {
            //audio stuff
            if (AudioDevice == null)
            {
                Enumerator = new MMDeviceEnumerator();
                AudioDevice = Enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            }

            float volume = AudioDevice.AudioMeterInformation.MasterPeakValue;

            return volume;
        }

        public static float GetAverageVolume()
        {
            float AverageVolume = TotalVolume / Divisor;
            return AverageVolume;
        }

    }
}
